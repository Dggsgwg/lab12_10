package com.example.lab12_10;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner);
        text = findViewById(R.id.text);
        final boolean[] canWrite = {false};

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(canWrite[0]) {
                    text.setText(getNewText((String) spinner.getItemAtPosition((int) id)));
                } else {
                    canWrite[0] = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private String getNewText(String value) {
        String result = text.getText().toString();
        if(result.isEmpty()) {
            return value;
        } else {
            return String.format("%s,%s", result, value);
        }
    }

    public void reset(View v) {
        String line = text.getText().toString();
        if(line.isEmpty()) return;
        if(!line.contains(",")) {
            text.setText("");
        } else {
            text.setText(line.substring(0, line.lastIndexOf(',')));
        }
    }
}